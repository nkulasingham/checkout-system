const { XforY, discount } = require('./defaultFunctions');

class DealFunctionRegistry {
  constructor(props, context) {
    this.functions = {};
  }

  add(name, functionCode) {
    this.functions[name] = functionCode;
  }
}

const DealRegistry = new DealFunctionRegistry();
DealRegistry.add('XforY', XforY);
DealRegistry.add('discount', discount);

module.exports = DealRegistry;
