function verify(input, { type, keys }) {
  if (!type || !keys) {
    throw new Error(
      'Verification function requires a type key and a keys key to be present in the options object ',
    );
  }
  if (typeof input !== type) {
    throw new Error(`input must be of type: ${type}`);
  }
  keys.forEach(key => {
    if (!input[key]) {
      throw new Error(`${key} key is required`);
    }
  });
  return true;
}

module.exports = verify;
