function modify(deal, items) {
  if (items.length < deal.x) {
    return items;
  }
  let counter = 0;
  items = items.map(val => {
    if (deal.x > counter) {
      counter++;
      return Object.assign({}, val, {
        price: (deal.y / deal.x) * val.price,
      });
    }
    return val;
  });
  return items;
}

function recurse(deal, items) {
  let newItems = items.slice(0, deal.x);
  newItems = modify(deal, newItems);
  let nextItems = items.slice(deal.x);
  if (nextItems.length) {
    nextItems = recurse(deal, nextItems);
  }
  return [...newItems, ...nextItems];
}

function XforY(deal, items) {
  let newItems = items;
  newItems = recurse(deal, items);
  return newItems;
}

function discount(deal, items) {
  return items.map(val => {
    return Object.assign({}, val, {
      price: deal.discountedPrice,
    });
  });
}

module.exports = { discount, XforY };
