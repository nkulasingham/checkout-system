const DealRegistry = require('./DealFunctionRegistry');

function applyDeal(deal, items) {
  let applicableItems = items.filter(item => item.identifier === deal.adType);
  return DealRegistry.functions[deal.dealType](deal, applicableItems);
}

function modifyPrices(items, deals) {
  let modifiedItems = [];
  const newItems = items;
  deals.forEach(deal => {
    modifiedItems = [...modifiedItems, ...applyDeal(deal, newItems)];
  });
  newItems.forEach(newItem => {
    modifiedItems.forEach(modifiedItem => {
      if (newItem.uuid === modifiedItem.uuid) {
        newItem.price = modifiedItem.price;
      }
    });
  });

  return newItems;
}

module.exports = modifyPrices;
