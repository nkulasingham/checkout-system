const ads = require('./data/ads.json');
const deals = require('./data/deals.json');
const Checkout = require('./src/Checkout');

/* Ideally in a proper application you would not have to pass 
in the customer as a paramers as the system would be able to 
identify the customer though a JWT or a cookie session but since 
this is meant to be a very barebones implementation, we need to 
identify the customer somehow */

const checkout = new Checkout({
  customer: 'Axil Coffee Roasters',
  inventory: ads,
  deals: deals,
});

checkout.add('standout');
checkout.add('standout');
checkout.add('standout');

checkout.add('premium');

const total = checkout.total();
console.log(total);
