// importing uuid for identification of items;
const uuid = require('uuid');

// importing clone to make a deep clone of an array of jsons;
const clone = require('ramda.clone');

const verify = require('../utils/verify');
const modifyPrices = require('../utils/modifyPrices');

class Checkout {
  constructor(params) {
    verify(params, {
      type: 'object',
      keys: ['inventory', 'customer', 'deals'],
    });

    Object.keys(params).forEach(key => {
      this[key] = params[key];
    });

    this.selectedItems = [];
    this.cartTotal = 0;
  }

  add(item) {
    const selectedItem = this.inventory.find(ad => ad.identifier === item);
    selectedItem &&
      this.selectedItems.push({ ...selectedItem, uuid: uuid.v1() });
    this.applyDeals();
  }

  applyDeals() {
    const applicableDeals = this.deals.filter(
      deal => deal.customer.toLowerCase() === this.customer.toLowerCase(),
    );
    const items = clone(this.selectedItems);
    const modifiedPrices = modifyPrices(items, applicableDeals);
    this.cartTotal = modifiedPrices
      .map(val => val.price)
      .reduce((prev, curr) => prev + curr);
  }

  total() {
    return this.cartTotal;
  }
}

module.exports = Checkout;
