const ads = require('../data/ads.json');
const deals = require('../data/deals.json');
const Checkout = require('../src/Checkout');

describe('Default Profile Tests', () => {
  test('Creates a new checkout object succesfully given correct parameters', () => {
    const checkout = new Checkout({
      customer: 'default',
      inventory: ads,
      deals: deals,
    });
    expect(checkout).toHaveProperty('customer');
    expect(checkout).toHaveProperty('inventory');
    expect(checkout).toHaveProperty('deals');
  });
  test('Adds item to checkout properly', () => {
    const checkout = new Checkout({
      customer: 'default',
      inventory: ads,
      deals: deals,
    });
    checkout.add('classic');
    expect(checkout).toHaveProperty('selectedItems');
    expect(checkout.selectedItems).toHaveLength(1);
  });
  test('Calculates total with one item properly', () => {
    const checkout = new Checkout({
      customer: 'default',
      inventory: ads,
      deals: deals,
    });
    checkout.add('classic');
    const total = checkout.total();
    expect(total).toEqual(269.99);
  });
  test('Calculates total with multiple items properly', () => {
    const checkout = new Checkout({
      customer: 'default',
      inventory: ads,
      deals: deals,
    });
    checkout.add('classic');
    checkout.add('standout');
    checkout.add('premium');
    const total = checkout.total();
    expect(total).toEqual(987.97);
  });
});
