const ads = require('../data/ads.json');
const deals = require('../data/deals.json');
const Checkout = require('../src/Checkout');

describe('Invalid Data Tests', () => {
  test('Throws an error if no params are passed in', () => {
    let error = undefined;
    try {
      const checkout = new Checkout();
    } catch (e) {
      error = e;
    }
    expect(error).toBeDefined();
    expect(error.message).toContain('input must be of type: object');
  });
  test('Throws an error if customer is falsy', () => {
    let error = undefined;
    try {
      const checkout = new Checkout({
        customer: '',
        inventory: ads,
        deals: deals,
      });
    } catch (e) {
      error = e;
    }
    expect(error).toBeDefined();
    expect(error.message).toContain('customer key is required');
  });
  test('Throws an error if inventory is falsy', () => {
    let error = undefined;
    try {
      const checkout = new Checkout({
        customer: 'default',
        inventory: undefined,
        deals: deals,
      });
    } catch (e) {
      error = e;
    }
    expect(error).toBeDefined();
    expect(error.message).toContain('inventory key is required');
  });
  test('Throws an error if deals is falsy', () => {
    let error = undefined;
    try {
      const checkout = new Checkout({
        customer: 'default',
        inventory: ads,
        deals: undefined,
      });
    } catch (e) {
      error = e;
    }
    expect(error).toBeDefined();
    expect(error).toBeDefined();
    expect(error.message).toContain('deals key is required');
  });
});
