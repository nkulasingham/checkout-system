const ads = require('../data/ads.json');
const deals = require('../data/deals.json');
const Checkout = require('../src/Checkout');

describe('Myer Tests', () => {
  test('Calculates total with multiple items properly', () => {
    const checkout = new Checkout({
      customer: 'MYER',
      inventory: ads,
      deals: deals,
    });
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    const total = checkout.total();
    expect(total).toEqual(1291.96);
  });
  test('Calculates total with 5 standout items properly', () => {
    const checkout = new Checkout({
      customer: 'MYER',
      inventory: ads,
      deals: deals,
    });
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('premium');
    const total = checkout.total();
    expect(total).toEqual(1681.95);
  });
  test('Calculates total with 10 standout items properly', () => {
    const checkout = new Checkout({
      customer: 'MYER',
      inventory: ads,
      deals: deals,
    });
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('premium');
    const total = checkout.total();
    expect(total).toEqual(2973.91);
  });
});
