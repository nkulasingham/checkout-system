const ads = require('./data/customAds.json');
const deals = require('./data/customDeals.json');
const Checkout = require('../src/Checkout');
const DealRegistry = require('../utils/DealFunctionRegistry');

describe('Custom Data Tests', () => {
  test('Calculates total with custom customers, deals and ads properly', () => {
    const checkout = new Checkout({
      customer: 'Browncoats',
      inventory: ads,
      deals: deals,
    });
    checkout.add('standard');
    checkout.add('standard');
    checkout.add('standard');
    const total = checkout.total();
    expect(total).toEqual(49.99);
  });
  test('Calculates total with a custom deal type properly', () => {
    function percentageOff(deal, items) {
      return items.map(val => {
        return Object.assign({}, val, {
          price: val.price - val.price * (deal.percentageOff / 100),
        });
      });
    }
    DealRegistry.add('percentageOff', percentageOff);
    const checkout = new Checkout({
      customer: 'Malcom Reynolds',
      inventory: ads,
      deals: deals,
    });
    checkout.add('vip');
    checkout.add('vip');
    checkout.add('vip');
    const total = checkout.total();
    expect(total).toEqual(89.991);
  });
});
