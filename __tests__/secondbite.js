const ads = require('../data/ads.json');
const deals = require('../data/deals.json');
const Checkout = require('../src/Checkout');

describe('Second Bite Profile Tests', () => {
  test('Calculates total with 3 of the same items properly', () => {
    const checkout = new Checkout({
      customer: 'SecondBite',
      inventory: ads,
      deals: deals,
    });
    checkout.add('classic');
    checkout.add('classic');
    checkout.add('classic');
    checkout.add('premium');
    const total = checkout.total();
    expect(total).toEqual(934.97);
  });
  test('Calculates total with 4 of the same items properly', () => {
    const checkout = new Checkout({
      customer: 'SecondBite',
      inventory: ads,
      deals: deals,
    });
    checkout.add('classic');
    checkout.add('classic');
    checkout.add('classic');
    checkout.add('classic');
    checkout.add('premium');
    const total = checkout.total();
    expect(total).toEqual(934.97 + 269.99);
  });
});
