const ads = require('../data/ads.json');
const deals = require('../data/deals.json');
const Checkout = require('../src/Checkout');

describe('Axil Coffee Roasters Tests', () => {
  test('Calculates total with multiple items properly', () => {
    const checkout = new Checkout({
      customer: 'Axil Coffee Roasters',
      inventory: ads,
      deals: deals,
    });
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('standout');
    checkout.add('premium');
    const total = checkout.total();
    expect(total).toEqual(1294.96);
  });
});
