# Checkout System

This is a simple checkout system.

Run `npm install` and then `npm test` to get the tests running. Run
`npm run coverage` to get coverage reports.

## Minimum Requirements

NodeJS 8+

NPM 5+

Unix based machine. Has not been tested on Windows but it should work fine on OSX / Linux.